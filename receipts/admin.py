from django.contrib import admin
from receipts.models import Receipt, ExpenseCategory, Account

# Register your models here.


class ReceiptAdmin(admin.ModelAdmin):
    pass


admin.site.register(Receipt, ReceiptAdmin)


class ExepenseCategoryAdmin(admin.ModelAdmin):
    pass


admin.site.register(ExpenseCategory, ExepenseCategoryAdmin)


class AccountAdmin(admin.ModelAdmin):
    pass


admin.site.register(Account, AccountAdmin)
